//
//  ViewController.swift
//  DemoLoginFB
//
//  Created by Doan Huy Binh on 9/29/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKLoginKit



class ViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var profilePictureView: FBSDKProfilePictureView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loginButton : FBSDKLoginButton = FBSDKLoginButton()
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loginButton)
        //
        let BottomConstraint = NSLayoutConstraint(item: loginButton, attribute: .bottom, relatedBy:.equal, toItem: self.view, attribute:.bottom, multiplier: 1.0, constant: -40)
        self.view.addConstraint(BottomConstraint)
        //
        let centerXConstraint = NSLayoutConstraint(item: loginButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        self.view.addConstraint(centerXConstraint)
        
        loginButton.delegate = self
       
        if FBSDKAccessToken.current() != nil{
            
            statusLabel.text = "You are logged in as"
            displayName()
        }
        else
        {
            statusLabel.text  = "You are logged out"
            nameLabel.text = ""
        }
        
    }
    func displayName()
    {
        let graphRequest: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        
        
        graphRequest.start { (connection, result, error) in
            let result = result as? NSDictionary
            let name = result?["name"] as! String
            self.nameLabel.text = name
        }
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        statusLabel.text = " You are Logged in as"
        displayName()
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        statusLabel.text = "You are Logged out"
        nameLabel.text = ""
    }
    
    @IBAction func enterApp(_ sender: AnyObject) {
        
        if FBSDKAccessToken.current() != nil
        {
            self.performSegue(withIdentifier: "mainApp", sender: self)
        }
        else
        {
            let alertController = UIAlertController(title: "Error", message: "You are Logged in to FaceBook", preferredStyle:.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

